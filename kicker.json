{
  "name": "semantic-release",
  "description": "GitLab CI template for semantic-release",
  "template_path": "templates/gitlab-ci-semrel.yml",
  "kind": "publish",
  "variables": [
    {
      "name": "SEMREL_IMAGE",
      "description": "The Docker image used to run semantic-release",
      "default": "node:lts"
    },
    {
      "name": "GITLAB_TOKEN",
      "description": "A GitLab [project access token](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html) or [personal access token](https://docs.gitlab.com/ce/user/profile/personal_access_tokens.html) with `api`, `read_repository` and `write repository` scopes.",
      "mandatory": true,
      "secret": true
    },
    {
      "name": "SEMREL_CONFIG_DIR",
      "description": "directory containing your [semantic-release configuration](https://semantic-release.gitbook.io/semantic-release/usage/configuration#configuration-file)",
      "default": ".",
      "advanced": true
    },
    {
      "name": "SEMREL_TAG_FORMAT",
      "description": "For generated `.releaserc` file only. [tagFormat semantic-release option](https://github.com/semantic-release/semantic-release/blob/master/docs/usage/configuration.md#tagformat)e. :warning: don't forget to double the `$` character so it is not interpreted by GitLab.",
      "default": "$${version}",
      "advanced": true
    },
    {
      "name": "SEMREL_REQUIRED_PLUGINS_FILE",
      "description": "Full path to `semrel-required-plugins.txt` file _(relative to `$CI_PROJECT_DIR`)_",
      "default": "semrel-required-plugins.txt",
      "advanced": true
    }
  ],
  "features": [
    {
      "id": "semantic-release",
      "name": "semantic-release",
      "description": "Performs a semantic release.",
      "variables": [
        {
          "name": "SEMREL_CHANGELOG_ENABLED",
          "description": "Add the [@semantic-release/changelog](https://github.com/semantic-release/changelog) plugin which will commit a changelog file in the repository.",
          "type": "boolean"
        },
        {
          "name": "SEMREL_CHANGELOG_FILE",
          "description": "[changelogFile @semantic-release/changelog option](https://github.com/semantic-release/changelog#options).",
          "default": "CHANGELOG.md",
          "advanced": true
        },
        {
          "name": "SEMREL_CHANGELOG_TITLE",
          "description": "[changelogTitle @semantic-release/changelog option](https://github.com/semantic-release/changelog#options). You might want to use markdown format (for example `# MyApp Changelog`).",
          "advanced": true
        },
        {
          "name": "SEMREL_DRY_RUN",
          "description": "For generated `.releaserc` file only. Activate the [dryRun semantic-release option](https://github.com/semantic-release/semantic-release/blob/master/docs/usage/configuration.md#dryrun) if present.",
          "type": "boolean",
          "advanced": true
        },
        {
          "name": "SEMREL_AUTO_RELEASE_ENABLED",
          "description": "When set the job start automatically. When not set (default), the job is manual.",
          "type": "boolean"
        },
        {
          "name": "SEMREL_HOOKS_DIR",
          "description": "Hook scripts folder.",
          "default": ".",
          "advanced": true
        },
        {
          "name": "SEMREL_RELEASE_DISABLED",
          "description": "Disable this job.",
          "advanced": true
        }
      ]
    },
    {
      "id": "semantic-release-info",
      "name": "semantic-release-info",
      "description": "Run `semantic-release` dry run to save next release version information as dotenv artifact and make them available in next pipeline stages",
      "variables": [
        {
          "name": "SEMREL_INFO_ON",
          "description": "Define on which branch(es) the job shall be run",
          "type": "enum",
          "values": ["", "prod", "protected", "all"],
          "mandatory": true
        }
      ]
    }
  ]
}
